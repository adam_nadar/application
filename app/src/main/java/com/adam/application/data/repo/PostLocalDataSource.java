package com.adam.application.data.repo;

import com.adam.application.data.model.Comments;
import com.adam.application.data.model.PostData;
import com.adam.application.data.model.Users;
import com.adam.util.callbak.DataCallback;

import java.util.List;

import javax.inject.Inject;

import io.objectbox.Box;
import io.objectbox.BoxStore;

public class PostLocalDataSource implements PostDataSource {

    private Box<PostData> postBox;
    private Box<Users> usersBox;
    private Box<Comments> commentBox;

    @Inject
    public PostLocalDataSource(BoxStore boxStore) {
        postBox = boxStore.boxFor(PostData.class);
        usersBox = boxStore.boxFor(Users.class);
        commentBox = boxStore.boxFor(Comments.class);
    }

    @Override
    public void loadPost(boolean forceRemote, DataCallback<List<PostData>, Throwable> dataCallback) {
        dataCallback.onSuccess(postBox.getAll());
    }

    @Override
    public void clearData() {
        postBox.removeAll();
    }

    @Override
    public void savePost(List<PostData> data) {
        postBox.put(data);
    }

    @Override
    public void getDetail(boolean forceRemote, long postId, DataCallback<Users, Throwable> dataCallback) {
        PostData obj = postBox.get(postId);
        long userId = obj.user.getTargetId();
        if (userId != 0)
            dataCallback.onSuccess(usersBox.get(userId));
        else
            dataCallback.onError(new Exception("local Copy Not Found"));
    }

    @Override
    public void getPostById(long postId, DataCallback<PostData, Throwable> dataCallback) {
        dataCallback.onSuccess(postBox.get(postId));
    }

    @Override
    public void saveUser(Users obj, long postId) {
        PostData data = postBox.get(postId);
        data.user.setTarget(obj);
        postBox.put(data);
        usersBox.put(obj);
    }

    @Override
    public void loadComments(boolean forceRemote, long postId, DataCallback<List<Comments>, Throwable> dataCallback) {
        PostData obj = postBox.get(postId);
        List<Comments> dataList = obj.comments;
        if (dataList == null || dataList.isEmpty()) {
            dataCallback.onError(new Exception("local Copy Not Found"));
        } else {
            dataCallback.onSuccess(dataList);
        }
    }

    @Override
    public void saveComment(List<Comments> obj, long postId) {
        PostData data = postBox.get(postId);
        for (Comments comment : obj)
            comment.postDataToOne.setTarget(data);
        commentBox.put(obj);
        postBox.put(data);
    }
}
