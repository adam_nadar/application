package com.adam.application.data.repo;

import com.adam.application.data.model.Comments;
import com.adam.application.data.model.PostData;
import com.adam.application.data.model.Users;
import com.adam.util.callbak.DataCallback;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class PostRepository implements PostDataSource {

    private PostDataSource remoteDataSource;
    private PostDataSource localDataSource;

    private List<PostData> caches;

    @Inject
    public PostRepository(@Local PostDataSource localDataSource,
                          @Remote PostDataSource remoteDataSource) {

        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;

        caches = new ArrayList<>();
    }

    @Override
    public void loadPost(boolean forceRemote, final DataCallback<List<PostData>, Throwable> dataCallback) {

        PostDataSource dataSource = forceRemote ? remoteDataSource : localDataSource;

        dataSource.loadPost(forceRemote, new DataCallback<List<PostData>, Throwable>() {
            @Override
            public void onSuccess(List<PostData> response) {
                localDataSource.savePost(response);
                caches.addAll(response);
                dataCallback.onSuccess(caches);
            }

            @Override
            public void onError(Throwable error) {
                dataCallback.onError(error);
                dataCallback.onSuccess(caches);
            }
        });

    }

    @Override
    public void clearData() {
        caches.clear();
        localDataSource.clearData();
    }

    @Override
    public void savePost(List<PostData> postList) {
        localDataSource.savePost(postList);
    }

    @Override
    public void getDetail(final boolean forceRemote, final long postId, final DataCallback<Users, Throwable> dataCallback) {
        PostDataSource dataSource = forceRemote ? remoteDataSource : localDataSource;

        dataSource.getDetail(forceRemote, postId, new DataCallback<Users, Throwable>() {
            @Override
            public void onSuccess(Users response) {
                if (forceRemote)
                    saveUser(response, postId);
                dataCallback.onSuccess(response);
            }

            @Override
            public void onError(Throwable error) {
                dataCallback.onError(error);
                if (!error.getLocalizedMessage().equals("")) {
                    if (!error.getLocalizedMessage().equals("local Copy Not Found"))
                        localDataSource.getDetail(false, postId, this);
                    else if (!error.getLocalizedMessage().equals("onFailure"))
                        remoteDataSource.getDetail(true, postId, this);
                }
            }
        });
    }

    @Override
    public void getPostById(long postId, final DataCallback<PostData, Throwable> dataCallback) {
        localDataSource.getPostById(postId, new DataCallback<PostData, Throwable>() {
            @Override
            public void onSuccess(PostData response) {
                dataCallback.onSuccess(response);
            }

            @Override
            public void onError(Throwable error) {
                dataCallback.onError(error);
            }
        });
    }

    @Override
    public void saveUser(Users obj, long postId) {
        localDataSource.saveUser(obj, postId);
    }

    @Override
    public void loadComments(final boolean forceRemote, final long postId, final DataCallback<List<Comments>, Throwable> dataCallback) {
        PostDataSource dataSource = forceRemote ? remoteDataSource : localDataSource;

        dataSource.loadComments(forceRemote, postId, new DataCallback<List<Comments>, Throwable>() {
            @Override
            public void onSuccess(List<Comments> response) {
                if (forceRemote)
                    saveComment(response, postId);
                dataCallback.onSuccess(response);
            }

            @Override
            public void onError(Throwable error) {
                if (!error.getLocalizedMessage().equals("")) {
                    if (!error.getLocalizedMessage().equals("local Copy Not Found"))
                        localDataSource.loadComments(false, postId, this);
                    else if (!error.getLocalizedMessage().equals("onFailure"))
                        remoteDataSource.loadComments(true, postId, this);
                }
            }
        });
    }

    @Override
    public void saveComment(List<Comments> obj, long postId) {
        localDataSource.saveComment(obj, postId);
    }
}
