package com.adam.application.data.repo;

import com.adam.application.data.model.Comments;
import com.adam.application.data.model.PostData;
import com.adam.application.data.model.Users;
import com.adam.util.callbak.DataCallback;

import java.util.List;

public interface PostDataSource {

    void loadPost(boolean forceRemote, DataCallback<List<PostData>, Throwable> dataCallback);

    void clearData();

    void savePost(List<PostData> postList);

    void getDetail(boolean forceRemote, long postId, DataCallback<Users, Throwable> dataCallback);

    void getPostById(long postId, DataCallback<PostData, Throwable> dataCallback);

    void saveUser(Users obj, long postId);

    void loadComments(boolean forceRemote, long postId, DataCallback<List<Comments>, Throwable> dataCallback);

    void saveComment(List<Comments> obj, long postId);
}
