package com.adam.application.data.api;

import com.adam.application.data.model.Comments;
import com.adam.application.data.model.PostData;
import com.adam.application.data.model.Users;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Services {
    @GET("posts")
    Call<List<PostData>> loadPosts();

    @GET("users/{userId}")
    Call<Users> getUserById(@Path("userId") long userId);

    @GET("comments")
    Call<List<Comments>> getCommentsByPostId(@Query("postId") long userId);
}
