package com.adam.application.data.model;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;
import io.objectbox.relation.ToOne;

@Entity
public class PostData {
    @Id(assignable = true)
    private long id;

    public ToOne<Users> user;

    @Backlink(to = "postDataToOne")
    public ToMany<Comments> comments;

    private String title;

    private String body;

    public PostData() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ToOne<Users> getUser() {
        return user;
    }

    public void setUser(ToOne<Users> user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
