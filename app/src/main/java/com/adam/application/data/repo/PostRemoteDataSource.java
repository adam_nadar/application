package com.adam.application.data.repo;

import android.support.annotation.NonNull;

import com.adam.application.data.api.Services;
import com.adam.application.data.model.Comments;
import com.adam.application.data.model.PostData;
import com.adam.application.data.model.Users;
import com.adam.util.callbak.DataCallback;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;

public class PostRemoteDataSource implements PostDataSource {

    private Services webService;

    @Inject
    public PostRemoteDataSource(Services webService) {
        this.webService = webService;
    }

    @Override
    public void loadPost(boolean forceRemote, final DataCallback<List<PostData>, Throwable> dataCallback) {
        webService.loadPosts().enqueue(new Callback<List<PostData>>() {
            @Override
            public void onResponse(@NonNull Call<List<PostData>> call, @NonNull Response<List<PostData>> response) {
                if (response.code() == HTTP_OK && response.body() != null)
                    dataCallback.onSuccess(response.body());
                else
                    dataCallback.onError(new Exception(""));
            }

            @Override
            public void onFailure(@NonNull Call<List<PostData>> call, @NonNull Throwable t) {
                dataCallback.onError(t);
            }
        });
    }

    @Override
    public void clearData() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void savePost(List<PostData> postList) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void getDetail(boolean forceRemote, long postId, final DataCallback<Users, Throwable> dataCallback) {
        webService.getUserById(postId).enqueue(new Callback<Users>() {
            @Override
            public void onResponse(@NonNull Call<Users> call, @NonNull Response<Users> response) {
                if (response.code() == HTTP_OK && response.body() != null) {
                    dataCallback.onSuccess(response.body());
                } else {
                    dataCallback.onError(new Exception(""));
                }
            }

            @Override
            public void onFailure(@NonNull Call<Users> call, @NonNull Throwable t) {
                dataCallback.onError(new Exception("onFailure"));
            }
        });
    }

    @Override
    public void getPostById(long postId, DataCallback<PostData, Throwable> dataCallback) {
        throw new UnsupportedOperationException("must Available in local dataSource");
    }

    @Override
    public void saveUser(Users obj, long postId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void loadComments(boolean forceRemote, long postId, final DataCallback<List<Comments>, Throwable> dataCallback) {
        webService.getCommentsByPostId(postId).enqueue(new Callback<List<Comments>>() {
            @Override
            public void onResponse(@NonNull Call<List<Comments>> call, @NonNull Response<List<Comments>> response) {
                if (response.code() == HTTP_OK && response.body() != null) {
                    dataCallback.onSuccess(response.body());
                } else {
                    dataCallback.onError(new Exception(""));
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Comments>> call, @NonNull Throwable t) {
                dataCallback.onError(new Exception("onFailure"));
            }
        });
    }

    @Override
    public void saveComment(List<Comments> obj, long postId) {
        throw new UnsupportedOperationException();
    }
}
