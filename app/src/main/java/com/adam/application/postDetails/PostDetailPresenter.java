package com.adam.application.postDetails;

import com.adam.application.data.model.Comments;
import com.adam.application.data.model.PostData;
import com.adam.application.data.model.Users;
import com.adam.application.data.repo.PostRepository;
import com.adam.util.callbak.DataCallback;

import java.util.List;

import javax.inject.Inject;

public class PostDetailPresenter implements DetailsContract.Presenter {

    private PostRepository repository;
    private DetailsContract.View view;

    @Inject
    public PostDetailPresenter(PostRepository repository, DetailsContract.View view) {
        this.repository = repository;
        this.view = view;
    }

    @Override
    public void getPostData(boolean onlineRequired, long postId) {
        view.loadingView(true);

        repository.getDetail(onlineRequired, postId, new DataCallback<Users, Throwable>() {
            @Override
            public void onSuccess(Users response) {
                view.setUser(response);
                view.loadingView(false);
            }

            @Override
            public void onError(Throwable error) {
                view.errorView();
                view.loadingView(false);
            }
        });

        repository.getPostById(postId, new DataCallback<PostData, Throwable>() {
            @Override
            public void onSuccess(PostData response) {
                view.setPost(response);
            }

            @Override
            public void onError(Throwable error) {

            }
        });

        repository.loadComments(onlineRequired, postId, new DataCallback<List<Comments>, Throwable>() {
            @Override
            public void onSuccess(List<Comments> response) {
                view.setComment(response.size());
            }

            @Override
            public void onError(Throwable error) {

            }
        });

    }
}
