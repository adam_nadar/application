package com.adam.application.postDetails;

import com.adam.application.data.model.PostData;
import com.adam.application.data.model.Users;

public class DetailsContract {

    public interface View {
        void setPost(PostData post);

        void setUser(Users obj);

        void loadingView(boolean isLoading);

        void errorView();

        void setComment(long size);
    }

    public interface Presenter {
        void getPostData(boolean onlineRequired, long postId);
    }
}
