package com.adam.application.postDetails;

import com.adam.application.data.repo.PostRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailsModule {

    @Provides
    DetailsContract.View providePostActView(DetailActivity detailAct) {
        return detailAct;
    }


    @Provides
    DetailsContract.Presenter providesListActPresenter(DetailsContract.View view, PostRepository repository) {
        return new PostDetailPresenter(repository, view);
    }
}
