package com.adam.application.postDetails;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.adam.application.R;
import com.adam.application.data.model.PostData;
import com.adam.application.data.model.Users;
import com.adam.application.databinding.ActivityDetailBinding;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class DetailActivity extends AppCompatActivity implements DetailsContract.View {

    @Inject
    DetailsContract.Presenter presenter;
    private ActivityDetailBinding binding;
    private long postId;

    public static void startDetail(Context context, PostData data) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra("postId", data.getId());
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        postId = getIntent().getLongExtra("postId", -1);
        setSupportActionBar(binding.toolbar);
        ActionBar bar = getSupportActionBar();
        if (bar != null)
            bar.setDisplayHomeAsUpEnabled(true);
        if (postId == -1) {
            Toast.makeText(this, "invalid post", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getPostData(false, postId);
    }

    @Override
    public void setPost(PostData post) {
        binding.toolbar.setTitle(post.getTitle());
        binding.body.setText(String.format(Locale.getDefault(), "Body : %s", post.getBody()));
    }

    @Override
    public void setUser(Users obj) {
        binding.userName.setText(String.format(Locale.getDefault(), "User Name : %s", obj.getUsername()));
        Picasso.get()
                .load("https://api.adorable.io/avatars/" + binding.toolbar.getHeight() + "/" + obj.getEmail())
                .into(binding.image, new Callback() {
                    @Override
                    public void onSuccess() {
                        binding.image.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError(Exception e) {
                        binding.image.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void loadingView(boolean isLoading) {
        binding.progress.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void errorView() {
        Snackbar.make(binding.getRoot(), R.string.offline_data_loaded, Snackbar.LENGTH_LONG)
                .setAction(R.string.lbl_retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.getPostData(true, postId);
                    }
                })
                .show();
    }

    @Override
    public void setComment(long size) {
        binding.comments.setText(String.format(Locale.getDefault(), "Number of Comments : %d", size));
    }
}