package com.adam.application.postList;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adam.application.R;
import com.adam.application.data.model.PostData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class PostListAdp extends RecyclerView.Adapter<PostListAdp.PostListHolder> {

    private List<PostData> data;
    private PostClickListener clickListener;

    @Inject
    PostListAdp(PostClickListener clickListener) {
        this.clickListener = clickListener;
        data = new ArrayList<>();
    }

    public void setData(@NonNull List<PostData> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (data.isEmpty())
            return 0;
        else
            return 1;
    }

    @NonNull
    @Override
    public PostListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if (viewType == 1) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(android.R.layout.simple_list_item_2, parent, false);
        } else {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        v.setPadding(20, 20, 20, 20);
        return new PostListHolder(v, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull PostListHolder holder, int position) {
        if (holder.getItemViewType() == 1) {
            holder.tv1.setText(data.get(position).getTitle());
            holder.tv2.setText(data.get(position).getBody());
        } else {
            holder.tv1.setText(R.string.swipe_to_refresh);
        }
    }

    @Override
    public int getItemCount() {
        if (data.isEmpty())
            return 1;
        return data.size();
    }

    interface PostClickListener {
        void showDetailsView(PostData postData);
    }

    class PostListHolder extends RecyclerView.ViewHolder {

        TextView tv1, tv2;

        PostListHolder(View itemView, int viewType) {
            super(itemView);
            tv1 = itemView.findViewById(android.R.id.text1);
            if (viewType == 1) {
                tv2 = itemView.findViewById(android.R.id.text2);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListener.showDetailsView(data.get(getAdapterPosition()));
                    }
                });
            }
        }
    }
}
