package com.adam.application.postList;

import com.adam.application.data.model.PostData;

import java.util.List;

public class PostActContract {

    public interface View {
        void setPostData(List<PostData> dataList);

        void loadingView(boolean isLoading);

        void errorView();
    }

    public interface Presenter {
        void getPostData(boolean onlineRequired);
    }
}
