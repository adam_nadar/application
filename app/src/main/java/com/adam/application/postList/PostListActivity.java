package com.adam.application.postList;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.adam.application.R;
import com.adam.application.data.model.PostData;
import com.adam.application.databinding.ActivityPostListBinding;
import com.adam.application.postDetails.DetailActivity;
import com.adam.util.SimpleDividerItemDecoration;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class PostListActivity extends AppCompatActivity implements PostActContract.View,
        SwipeRefreshLayout.OnRefreshListener, PostListAdp.PostClickListener {

    @Inject
    PostListAdp adp;
    @Inject
    PostActContract.Presenter presenter;
    private ActivityPostListBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_list);

        binding.swipe.setColorSchemeResources
                (android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                        android.R.color.holo_orange_light, android.R.color.holo_red_light);

        setSupportActionBar(binding.toolbar);
        binding.swipe.setOnRefreshListener(this);
        SimpleDividerItemDecoration itemDecor = new SimpleDividerItemDecoration(this);
        binding.postList.addItemDecoration(itemDecor);
        binding.postList.setAdapter(adp);
        binding.postList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getPostData(false);
    }

    @Override
    public void setPostData(List<PostData> dataList) {
        adp.setData(dataList);
        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);
    }

    @Override
    public void loadingView(boolean isLoading) {
        binding.swipe.setRefreshing(isLoading);
    }

    @Override
    public void errorView() {
        Snackbar.make(binding.getRoot(), R.string.offline_data_loaded, Snackbar.LENGTH_SHORT)
                .setAction(R.string.lbl_retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onRefresh();
                    }
                })
                .show();
        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);
    }

    @Override
    public void showDetailsView(PostData postData) {
        DetailActivity.startDetail(this, postData);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter.getPostData(true);
            }
        }, 1000);
    }

}
