package com.adam.application.postList;

import com.adam.application.data.model.PostData;
import com.adam.application.data.repo.PostRepository;
import com.adam.util.callbak.DataCallback;

import java.util.List;

import javax.inject.Inject;

public class PostPresenter implements PostActContract.Presenter, DataCallback<List<PostData>, Throwable> {

    private PostRepository repository;
    private PostActContract.View view;

    @Inject
    public PostPresenter(PostRepository repository, PostActContract.View view) {
        this.repository = repository;
        this.view = view;
    }

    @Override
    public void getPostData(boolean onlineRequired) {
        view.loadingView(true);
        repository.loadPost(onlineRequired, this);
    }

    @Override
    public void onSuccess(List<PostData> response) {
        view.setPostData(response);
        view.loadingView(false);
    }

    @Override
    public void onError(Throwable error) {
        view.loadingView(false);
        view.errorView();
    }
}