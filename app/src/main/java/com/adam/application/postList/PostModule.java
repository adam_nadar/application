package com.adam.application.postList;

import com.adam.application.data.repo.PostRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class PostModule {

    @Provides
    PostActContract.View providePostActView(PostListActivity listAct) {
        return listAct;
    }

    @Provides
    PostActContract.Presenter providesListActPresenter(PostActContract.View view, PostRepository repository) {
        return new PostPresenter(repository, view);
    }

    @Provides
    PostListAdp.PostClickListener providesPostClickListener(PostListActivity listAct) {
        return listAct;
    }

    @Provides
    PostListAdp providesPostListAdp(PostListAdp.PostClickListener listener) {
        return new PostListAdp(listener);
    }

}
