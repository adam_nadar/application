package com.adam.application.app;

import android.app.Activity;
import android.app.Application;

import com.adam.application.BuildConfig;
import com.adam.application.app.di.DaggerAppComponent;
import com.facebook.stetho.Stetho;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import io.objectbox.BoxStore;
import io.objectbox.android.AndroidObjectBrowser;

public class AppController extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;
    @Inject
    BoxStore boxStore;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerAppComponent
                .builder()
                .application(this)
                .build()
                .inject(this);

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
            new AndroidObjectBrowser(boxStore).start(this);
        }
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }
}