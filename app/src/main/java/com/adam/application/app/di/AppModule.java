package com.adam.application.app.di;

import android.app.Application;
import android.content.Context;

import com.adam.application.BuildConfig;
import com.adam.application.MyObjectBox;
import com.adam.application.data.api.Services;
import com.adam.application.data.model.PostData;
import com.adam.application.data.repo.Local;
import com.adam.application.data.repo.PostDataSource;
import com.adam.application.data.repo.PostLocalDataSource;
import com.adam.application.data.repo.PostRemoteDataSource;
import com.adam.application.data.repo.PostRepository;
import com.adam.application.data.repo.Remote;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.objectbox.Box;
import io.objectbox.BoxStore;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(Application context) {
        return context;
    }

    @Provides
    @Singleton
    BoxStore providesBoxStore(Application application) {
        return MyObjectBox.builder().androidContext(application).build();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(GsonConverterFactory gsonConverterFactory,
                              OkHttpClient okHttpClient) {
        return new Retrofit.Builder().baseUrl(BuildConfig.SERVER_URL)
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient(Cache cache) {
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .cache(cache)
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG)
            client.addNetworkInterceptor(new StethoInterceptor());

        return client.build();
    }

    @Provides
    @Singleton
    Cache providesOkhttpCache(Context context) {
        int cacheSize = 10 * 1024 * 1024; // 10 MB
        return new Cache(context.getCacheDir(), cacheSize);
    }

    @Provides
    @Singleton
    Gson providesGson() {
        return new Gson();
    }

    @Provides
    Services provideWebService(Retrofit retrofit) {
        return retrofit.create(Services.class);
    }

    @Provides
    @Singleton
    GsonConverterFactory providesGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @Singleton
    Box<PostData> providesPostBox(BoxStore boxStore) {
        return boxStore.boxFor(PostData.class);
    }

    @Provides
    @Singleton
    PostRepository providesPostRepository(@Local PostDataSource postLocalDataSource, @Remote PostDataSource postRemoteDataSource) {
        return new PostRepository(postLocalDataSource, postRemoteDataSource);
    }

    @Local
    @Provides
    @Singleton
    PostDataSource providesLocalPostDataSource(BoxStore boxStore) {
        return new PostLocalDataSource(boxStore);
    }

    @Remote
    @Provides
    @Singleton
    PostDataSource providesRemotePostDataSource(Services services) {
        return new PostRemoteDataSource(services);
    }
}
