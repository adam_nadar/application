package com.adam.application.app.di;

import com.adam.application.postDetails.DetailActivity;
import com.adam.application.postDetails.DetailsModule;
import com.adam.application.postList.PostListActivity;
import com.adam.application.postList.PostModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = PostModule.class)
    abstract PostListActivity contributeListAct();

    @ContributesAndroidInjector(modules = DetailsModule.class)
    abstract DetailActivity contributeDetailAct();
}