package com.adam.util.callbak;

public interface DataCallback<R, E> {
    void onSuccess(R response);

    void onError(E error);
}